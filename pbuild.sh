#!/bin/bash

# Config and files are set to be used system-wide, allowing access to multiple users.
LANG=C
usage="pbuild.sh RELEASE [-u]"
help="  RELEASE\tall|bionic|focal\n\nBuild a package from the current directory using pbuilder."
base_dir="/var/cache/pbuilder"
result_dir="${base_dir}/result"
do_mirror="http://mirrors.digitalocean.com/ubuntu/"
components="main universe multiverse restricted"

# Handle arguments.
dist="$1"
if [[ -z "$dist" ]]; then
    echo "$usage"
    exit 1
elif [[ "$dist" == '-h' ]] || [[ "$dist" == '--help' ]]; then
    echo -e "${usage}\n\n${help}"
    exit 0
elif [[ "$dist" != 'bionic' && "$dist" != 'focal' && "$dist" != 'jammy' ]]; then
    echo -e "Error: Supported releases: bionic|focal|jammy"
    exit 1
fi

# Ensure that pbuilder is installed.
if [[ ! -x /usr/sbin/pbuilder ]]; then
    sudo apt-get update
    sudo apt-get install --yes pbuilder
fi

# Ensure that rsyncd is installed and configured.
if [[ ! -x /usr/bin/rsync ]]; then
    sudo apt-get update
    sudo apt-get install --yes rsync
fi
rsyncd_conf="/etc/rsyncd.conf"
config="
[debs]
    path = $result_dir
    comment = experimental debian packages
    read only = true
    max connections = 5
    dont compress = *.gz *.tgz *.zip *.z *.Z *.rpm *.deb *.bz2

"
# Check config.
if [[ ! -f "$rsyncd_conf" ]]; then
    sudo touch "$rsyncd_conf"
fi
if [[ ! $(grep "$result_dir" "$rsyncd_conf") ]]; then
    echo "$config" | sudo tee -a "$rsyncd_conf"
fi
# Check daemon.
if [[ $(systemctl is-active rsync.service) != 'active' ]]; then
    sudo systemctl start rsync.service
fi

# Ensure basetgz.
basetgz="${base_dir}/${dist}-base.tgz"
if [[ ! -e "$basetgz" ]]; then
    sudo pbuilder create \
        --distribution "$dist" \
        --mirror "$do_mirror" \
        --components "$components" \
        --basetgz "$basetgz"
    if [[ $? -ne 0 ]]; then
        echo "ERROR: Failed to create $basetgz"
        exit 1
    fi
fi

# Run pbuilder command.
buildresult="${base_dir}/result/${dist}"
if [[ ! -z "$2" ]] && [[ "$2" == '-u' ]]; then
    # Update the base.
    sudo pbuilder update --basetgz "$basetgz"
else
    # Build the package.
    sudo mkdir -p "$buildresult"
    # sudo mkdir -p "$buildresult" # shouldn't be necessary
    # sudo pbuilder debuild --buildresult "$buildresult" --basetgz "$basetgz" # incomplete
    sudo pdebuild --use-pdebuild-internal --buildresult "$buildresult" -- --basetgz "$basetgz"
fi
