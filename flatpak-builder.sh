#!/bin/bash

# Build flatpak package in a 64-bit container.

function container_exists() {
    lxc --project "$project" list | grep -q "$1"
}

function run_cmd() {
    lxc --project "$project" exec "$container_name" -- "$@"
}

function container_deb_is_installed() {
    if [[ -n "$(run_cmd dpkg-query --show "$1" | tr -s ' ' | awk '{print $2}')" ]]; then
        return 0
    else
        return 1
    fi
}

function container_deb_install() {
    run_cmd apt-get install --yes --no-install-recommends "$1"
}

function container_flatpak_is_installed() {
    run_cmd flatpak list | grep -qw "$1"
}

function container_flatpak_install() {
    run_cmd flatpak install --assumeyes flathub "$1"
}

function get_flatpak_config() {
    if [[ ! -r "$fp_config" ]]; then
        echo "Error: File not readable: $fp_config"
        exit 1
    fi
    grep "$1" "$fp_config" | awk -F':' '{print $2}' | sed 's/^\s*//'
}

function get_lxc_config() {
    if [[ ! -r "$lxc_cfg" ]]; then
        echo "Error: File not readable: $lxc_cfg"
        exit 1
    fi
    grep "^$1" "$lxc_cfg" | awk -F= '{print $2}'
}

project='flatpak'
script="$(realpath "$0")"
repo_dir="$(dirname "$script")"
data_dir="${repo_dir}/data"

# Get container config.
lxc_cfg="${PWD}/flatpak/lxc.cfg"
buildimage="$(get_lxc_config "buildimage")"
if [[ -z "$buildimage" ]]; then
    echo "Error: buildimage not found in $lxc_cfg"
    exit 1
fi
build_pkgs="$(get_lxc_config "build_pkgs")"

# Get flatpak config.
fp_config="$(find . -wholename '*/flatpak/*.yml')"
name="$(get_flatpak_config "app\-id:")"
if [[ -z "$name" ]]; then
    echo "Error: flatpak name not found."
    exit 1
fi
container_name="$(echo "$name" | tr '.' '-')" # must be alphanumeric + '-'

# Handle options.
show_usage() { echo "Usage: $0 [-d] [-h|-l|-s|-c <command>]" 1>&2; }
while getopts "c:dhlr" o; do
    case "$o" in
        c) # command
            run_cmd $OPTARG
            exit $?
            ;;
        d) # debug
            set -x
            ;;
        h) # help
            show_usage
            exit 0
            ;;
        l) # List lxc containers.
            lxc --project "$project" list
            exit $?
            ;;
        r) # Remove lxc container.
            if ! container_exists "$container_name"; then
                echo "No container image to remove. Check \"lxc --project $project list\" for base containers."
                exit 0
            else
                echo "Stopping and deleting container: $container_name"
                lxc --project "$project" stop "$container_name"
                lxc --project "$project" delete "$container_name"
                exit $?
            fi
            ;;
        *)
            show_usage
            exit 1
            ;;
    esac
done
shift $((OPTIND-1))

# Set flatpak variables.
fp_runtime="$(get_flatpak_config "runtime:")"
fp_runtime_version="$(get_flatpak_config "runtime\-version:" | tr -d "'")"
fp_sdk="${fp_runtime%.*}.Sdk"

# Ensure installation of lxd.
if [[ ! $(which lxd) ]]; then
    echo "Installing lxd..."
    sudo snap install lxd
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to install lxd snap. Exiting."
        exit 1
    fi
    # Add user to lxd group.
    sudo usermod -a -G lxd "$(whoami)"
fi

# Ensure project creation.
if ! lxc project list --format csv | grep -q "^$project"; then
    lxc project create "$project"
fi

# Ensure project profile.
if ! lxc --project "$project" profile list --format csv | grep -q "^$project"; then
    # Create profile.
    lxc --project "$project" profile create "$project"
fi

# Update profile config according to YAML.
profile_cfg="${data_dir}/${project}-profile.yaml"
if [[ ! -r "$profile_cfg" ]]; then
    echo "Error: File not readable: $profile_cfg"
    exit 1
fi
cat "$profile_cfg" | lxc --project "$project" profile edit "$project"

# Ensure container creation.
if ! container_exists "$container_name"; then
    echo "Creating $project build container: $container_name"
    # lxc --project "$project" profile show "$project" | lxd init --preseed
    # lxd init --minimal
    lxc --project "$project" launch images:"$buildimage" \
        --storage default "$container_name" \
        --network lxdbr0
fi

# Ensure container setup.
base_debs=(
    'flatpak'
    'policykit-1'
)
for deb in "${base_debs[@]}"; do
    if ! container_deb_is_installed "$deb"; then
        container_deb_install "$deb"
    fi
done
# Add flathub repo.
run_cmd flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Ensure build setup.
for deb in $build_pkgs; do
    if ! container_deb_is_installed "$deb"; then
        container_deb_install "$deb"
    fi
done
# Install flatpaks.
if ! container_flatpak_is_installed "org.flatpak.Builder"; then
    container_flatpak_install "org.flatpak.Builder"
fi
runtime_flatpaks=(
    "$fp_runtime"
    "$fp_sdk"
)
for pak in "${runtime_flatpaks[@]}"; do
    if ! container_flatpak_is_installed "$pak"; then
        container_flatpak_install "${pak}//${fp_runtime_version}"
    fi
done

# Run flatpak build command chain.
run_cmd flatpak run org.flatpak.Builder \
    --disable-rofiles-fuse \
    --force-clean \
    build "$fp_config"
