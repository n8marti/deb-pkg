#!/bin/bash

debug=
publish=
snap_name=$(find . -name snapcraft.yaml -exec cat {} \; | grep name: | awk '{print $2}')
container_names=$(lxc --project snapcraft list --format csv | awk -F',' '{print $1}')
container_name=$(lxc --project snapcraft list --format csv | grep "$snap_name" | awk -F',' '{print $1}')
# Set mem limit to 75% of available.
mem_total=$(free -b | grep Mem: | awk '{print $2}')  # -L option n/a in 22.04
lxc_mem_limit="$((mem_total*3/4))"  # sometimes get OOM error, snapcraft exit code 137
# if [[ -n $(swapon -s) ]]; then
#     lxc_mem_limit="800MB"
# fi

usage() {
    echo "Usage: $0 [-r | -h] | [-d] [-- SNAPCRAFT_OPTS...]"
}

remove_container() {
    if [[ -z "$1" ]]; then
        echo "No container image to remove. Check \"lxc --project snapcraft list\" for base containers."
        return 1
    else
        echo "Stopping and deleting container: $1"
        lxc --project snapcraft stop "$1"
        lxc --project snapcraft delete "$1"
        return $?
    fi
}

while getopts ":hlr" o; do
    case "$o" in
        h)
            usage
            exit 0
            ;;
        l)
            lxc --project snapcraft list
            exit $?
            ;;
        r)
            remove_container $container_name
            exit $?
            ;;
    esac
done
shift $((OPTIND-1))

# Ensure snapcraft.
if ! which snapcraft 2>/dev/null 1>&2; then
    snap install snapcraft --classic
fi

# Ensure rsyncd.
repo_dir="$(dirname "$0")"
"${repo_dir}/ensure-rsyncd.sh"

# Set debug according to snapcraft args.
if [[ $* =~ .*--debug.* ]]; then
    debug=1
fi

# Set publish.
if [[ -z $debug && "$1" != 'clean' ]]; then
    publish=1
fi

# Ensure initialization of lxd.
if [[ -z "$container_name" ]]; then
    echo "Ensuring lxd initialization..."
    lxd init --auto
    if [[ $? -ne 0 ]]; then
        echo -e "\nFailed to init lxd. Exiting."
        exit 1
    fi
fi

# Ensure memory limits for lxd container.
for c in $container_names; do
    current_mem_enforcement=$(lxc --project snapcraft config get "$c" limits.memory.enforce)
    current_mem_limit=$(lxc --project snapcraft config get "$c" limits.memory)
    if [[ "$current_mem_enforcement" != 'hard' ]]; then
        lxc --project snapcraft config set "$c" limits.memory.enforce "hard"
    fi
    if [[ "$current_mem_limit" != "$lxc_mem_limit" ]]; then
        lxc --project snapcraft config set "$c" limits.memory "$lxc_mem_limit"
    fi
done

# Remove old snap builds.
rm -f ./*.snap

# Set ENV before build.
export LANG=C.UTF-8
export SNAPCRAFT_BUILD_ENVIRONMENT=lxd
# export SNAPCRAFT_BUILD_ENVIRONMENT_MEMORY=570M # only for multipass
# export SNAPCRAFT_BUILD_ENVIRONMENT_DISK=5G # only for multipass

# Run snapcraft command chain.
snapcraft "$@"
ec=$?

# Exit now if run with 'debug'.
if [[ -n $debug ]]; then
    echo "snapcraft run in debug mode, not trying to copy or upload package."
    exit $ec
fi

# Publish snap (copy to rsyncd snaps directory & push to Snap Store).
if [[ $ec -eq 0 && -n $publish ]]; then
    rsync -tuv ./*.snap /srv/rsync
    if snapcraft whoami >/dev/null 2>&1; then
        snapcraft upload *.snap --release=edge
    fi
fi
